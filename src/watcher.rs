// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::io::Result;
use std::path::Path;

use futures::Stream;
use inotify::{EventOwned, Inotify, WatchMask};

/// A watcher of a directory for new files.
pub(crate) struct Watcher {
    /// The internal inotify watcher.
    ino: Inotify,
}

impl Watcher {
    /// Watch a given directory.
    pub(crate) fn new(path: &Path) -> Result<Self> {
        let ino = Inotify::init()?;
        ino.watches().add(
            path,
            WatchMask::CLOSE_WRITE | WatchMask::MOVED_TO | WatchMask::ONLYDIR,
        )?;

        Ok(Self {
            ino,
        })
    }

    /// Block until events are returned.
    pub(crate) fn events(self) -> Result<impl Stream<Item = Result<EventOwned>>> {
        // Create a buffer on the heap for use within the event stream machinery.
        let vec = Box::new([0; 1024]);
        // Alas, it must have a `'static` liftime, so we are forced to leak it to make it useful.
        self.ino.into_event_stream(Box::leak(vec))
    }
}
